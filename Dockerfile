FROM bash
 
RUN apk update
RUN apk add gzip git w3m openssh-client
RUN wget https://github.com/boyter/scc/releases/download/v3.1.0/scc_3.1.0_Linux_x86_64.tar.gz -O scc.gz



 CMD ["git", "gzip", "w3m", "ssh"]